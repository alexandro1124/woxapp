package woxapp.com.woxapptestapplication.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import woxapp.com.woxapptestapplication.BuildConfig;
import woxapp.com.woxapptestapplication.R;
import woxapp.com.woxapptestapplication.helper.entitiy.CombinedTurnover;
import woxapp.com.woxapptestapplication.helper.entitiy.Reminder;
import woxapp.com.woxapptestapplication.helper.entitiy.UserDB;
import woxapp.com.woxapptestapplication.helper.entitiy.WineInStock;
import woxapp.com.woxapptestapplication.model.adapters.ReminderAdapter;
import woxapp.com.woxapptestapplication.model.adapters.TurnOverAdapter;
import woxapp.com.woxapptestapplication.presenter.MainPresenter;

public class MainActivity extends AppCompatActivity implements TurnOverAdapter.GetTurnoverClickItem, ReminderAdapter.GetReminderClickItem {

    private boolean mTwoPane;
    private Toolbar mToolbar;
    private DrawerLayout drawer;
    MainPresenter mainPresenter = new MainPresenter(this);
    private static final String TAG = MainActivity.class.getSimpleName();
    @BindView(R.id.total)
    TextView total;
    @BindView(R.id.inbox)
    TextView inbox;
    @BindView(R.id.bottle)
    TextView bottle;
    @BindView(R.id.turnoverRV)
    RecyclerView turnoverRV;
    @BindView(R.id.reminderRV)
    RecyclerView reminderRV;
    @BindView(R.id.emptyNotifView)
    RelativeLayout emptyNotifView;
    @BindView(R.id.emptyStatView)
    RelativeLayout emptyStatView;
    @BindView(R.id.emptyWineView)
    RelativeLayout emptyWineView;
    @BindView(R.id.emptyNotifText)
    TextView emptyNotifText;
    @BindView(R.id.emptyRemStat)
    TextView emptyRemStat;
    @BindView(R.id.emptyWineText)
    TextView emptyWineText;
    @BindView(R.id.progressBarNotif)
    ProgressBar progressBarNotif;
    @BindView(R.id.progressBarStat)
    ProgressBar progressBarStat;
    @BindView(R.id.progressBarWine)
    ProgressBar progressBarWine;
    @BindView(R.id.wineView)
    RelativeLayout wineView;

    @OnClick(R.id.addWine)
    public void submit(View view) {
        Toast.makeText(getApplicationContext(), "add", Toast.LENGTH_LONG).show();
        mainPresenter.showDbData(String.valueOf(mainPresenter.getUserDb().getId()));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mainPresenter.onCreate();
        setUpToolbar();
        if (findViewById(R.id.drawer) != null) {//Phone layout
            mTwoPane = false;
            setUpNavDrawer();
        } else {//Tablet layout
            mTwoPane = true;
        }

        if (getIntent() != null) {
            Intent intent = getIntent();
            if (intent.hasExtra("userDB")) {
                UserDB userDB = intent.getParcelableExtra("userDB");
                mainPresenter.setUserDb(userDB);
            }
        }
        setUpTurnoverRV();
        setUpReminderRV();


        mainPresenter.checkAllData();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
        if (!mTwoPane && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void setUpTurnoverRV() {
        TurnOverAdapter turnOverAdapter = new TurnOverAdapter(this, this);
        turnoverRV.setAdapter(turnOverAdapter);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        turnoverRV.setLayoutManager(layoutManager);
        turnoverRV.setHasFixedSize(true);
    }

    private void setUpReminderRV() {
        ReminderAdapter reminderAdapter = new ReminderAdapter(this, this);
        reminderRV.setAdapter(reminderAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        reminderRV.setLayoutManager(layoutManager);
        reminderRV.setHasFixedSize(true);
    }

    public void showWineInStock(WineInStock wineInStock) {

        if (!TextUtils.isEmpty(wineInStock.getTotal()))
            total.setText(wineInStock.getTotal());
        if (!TextUtils.isEmpty(wineInStock.getBottle()))
            bottle.setText(wineInStock.getBottle());
        if (!TextUtils.isEmpty(wineInStock.getInbox()))
            inbox.setText(wineInStock.getInbox());
    }

    public void showRemaining(List<Reminder> reminderList) {

        ReminderAdapter contentAdapter = (ReminderAdapter) reminderRV.getAdapter();
        if (contentAdapter.getItemCount() > 0) {
            contentAdapter.clear();
        }
        contentAdapter.add(reminderList);
    }

    public void showTurnOver(List<CombinedTurnover> turnoverList) {

        TurnOverAdapter contentAdapter = (TurnOverAdapter) turnoverRV.getAdapter();
        if (contentAdapter.getItemCount() > 0) {
            contentAdapter.clear();
        }
        contentAdapter.add(turnoverList);
    }

    public void showErrorMessage(String message) {
        View rootview = findViewById(android.R.id.content);
        if (rootview != null) {
            Snackbar.make(rootview, message, Snackbar.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }
    }

    public void showProgresWineView(final boolean show) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            showProgress(show, wineView, emptyWineView);
        } else {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showProgress(show, wineView, emptyWineView);
                }
            });
        }

    }

    public void showProgresRemView(final boolean show) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            showProgress(show, reminderRV.getRootView(), emptyNotifView);
        } else {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showProgress(show, reminderRV.getRootView(), emptyNotifView);
                }
            });
        }

    }

    public void showProgresTurnView(final boolean show) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            showProgress(show, turnoverRV.getRootView(), emptyStatView);
        } else {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showProgress(show, turnoverRV.getRootView(), emptyStatView);
                }
            });
        }
    }

    public void changeLoadTextWine(String text) {
        setEmtyViewText(emptyWineText, text);
    }

    public void changeLoadTextRem(String text) {
        setEmtyViewText(emptyNotifText, text);
    }

    public void changeLoadTextStat(String text) {
        setEmtyViewText(emptyRemStat, text);
    }

    /**
     * Shows the progress UI and hides the some of 3 form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show, final View mFormView, final RelativeLayout mProgressView) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "show loadingview is " + show);
        }
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public void setEmtyViewText(final TextView textView, final String message) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            textView.setText(message);
        } else {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textView.setText(message);
                }
            });
        }

    }


    private void setUpNavDrawer() {
        if (mToolbar != null) {
            drawer = (DrawerLayout) findViewById(R.id.drawer);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            toggle.syncState();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.openDrawer(GravityCompat.START);
                }
            });
        }
    }

    private void setUpToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
    }


    @Override
    public void onRemItemClick(View v, int pos) {

    }

    @Override
    public void onTurnItemClick(View v, int pos) {

    }
}
