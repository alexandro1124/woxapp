package woxapp.com.woxapptestapplication.helper;

/**
 * Created by Alexandro on 23.10.2016.
 */

public interface LoadCallback {

    enum LoadCode {EmptyData, LoadError,NoError}

    public void loadSuccess(String data);

    public void loadError(LoadCode code, String errorMessage);
}
