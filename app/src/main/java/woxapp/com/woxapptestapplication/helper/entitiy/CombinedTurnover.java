package woxapp.com.woxapptestapplication.helper.entitiy;

/**
 * Created by Alexandro on 27.10.2016.
 */

public class CombinedTurnover {

    private Turnover coming;
    private Turnover consumption;

    public Turnover getComing() {
        return coming;
    }

    public void setComing(Turnover coming) {
        this.coming = coming;
    }

    public Turnover getConsumption() {
        return consumption;
    }

    public void setConsumption(Turnover consumption) {
        this.consumption = consumption;
    }
}
