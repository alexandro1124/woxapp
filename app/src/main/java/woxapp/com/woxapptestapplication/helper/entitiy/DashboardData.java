package woxapp.com.woxapptestapplication.helper.entitiy;

import java.util.List;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class DashboardData {

    private WineInStock wineInStock;
    private List<Reminder> reminder;
    private  List<Turnover> turnover;

    public WineInStock getWineInStock() {
        return wineInStock;
    }

    public void setWineInStock(WineInStock wineInStock) {
        this.wineInStock = wineInStock;
    }

    public List<Reminder> getReminder() {
        return reminder;
    }

    public void setReminder(List<Reminder> reminder) {
        this.reminder = reminder;
    }

    public List<Turnover> getTurnover() {
        return turnover;
    }

    public void setTurnover(List<Turnover> turnover) {
        this.turnover = turnover;
    }
}
