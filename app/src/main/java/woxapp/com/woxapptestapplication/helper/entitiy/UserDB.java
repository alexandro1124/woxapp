package woxapp.com.woxapptestapplication.helper.entitiy;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class UserDB implements Parcelable {
    private long id;

    public UserDB() {
    }

    protected UserDB(Parcel in) {
        id = in.readLong();
        token = in.readString();
        cellarId = in.readString();
        imei = in.readString();
    }

    public static final Creator<UserDB> CREATOR = new Creator<UserDB>() {
        @Override
        public UserDB createFromParcel(Parcel in) {
            return new UserDB(in);
        }

        @Override
        public UserDB[] newArray(int size) {
            return new UserDB[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    private String token, cellarId, imei;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCellarId() {
        return cellarId;
    }

    public void setCellarId(String cellarId) {
        this.cellarId = cellarId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(token);
        dest.writeString(cellarId);
        dest.writeString(imei);
    }
}
