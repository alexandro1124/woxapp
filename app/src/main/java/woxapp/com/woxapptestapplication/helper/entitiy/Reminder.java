package woxapp.com.woxapptestapplication.helper.entitiy;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class Reminder implements Parcelable{

    @SerializedName("id")
    private String id;
    @SerializedName("canaryId")
    private String canaryId;
    @SerializedName("date")
    private String date;
    @SerializedName("wineName")
    private String wineName;
    @SerializedName("boxCount")
    private String boxCount;
    @SerializedName("bottleCount")
    private String bottleCount;
    @SerializedName("text")
    private String text;
    @SerializedName("reminderType")
    private String reminderType;
    @SerializedName("userId")
    private String userId;

    public Reminder(){}
    protected Reminder(Parcel in) {
        id = in.readString();
        canaryId = in.readString();
        date = in.readString();
        wineName = in.readString();
        boxCount = in.readString();
        bottleCount = in.readString();
        text = in.readString();
        reminderType = in.readString();
        userId = in.readString();
    }

    public static final Creator<Reminder> CREATOR = new Creator<Reminder>() {
        @Override
        public Reminder createFromParcel(Parcel in) {
            return new Reminder(in);
        }

        @Override
        public Reminder[] newArray(int size) {
            return new Reminder[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCanaryId() {
        return canaryId;
    }

    public void setCanaryId(String canaryId) {
        this.canaryId = canaryId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getWineName() {
        return wineName;
    }

    public void setWineName(String wineName) {
        this.wineName = wineName;
    }

    public String getBoxCount() {
        return boxCount;
    }

    public void setBoxCount(String boxCount) {
        this.boxCount = boxCount;
    }

    public String getBottleCount() {
        return bottleCount;
    }

    public void setBottleCount(String bottleCount) {
        this.bottleCount = bottleCount;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getReminderType() {
        return reminderType;
    }

    public void setReminderType(String reminderType) {
        this.reminderType = reminderType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(canaryId);
        dest.writeString(date);
        dest.writeString(wineName);
        dest.writeString(boxCount);
        dest.writeString(bottleCount);
        dest.writeString(text);
        dest.writeString(reminderType);
        dest.writeString(userId);
    }
}
