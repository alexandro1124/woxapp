package woxapp.com.woxapptestapplication.helper.entitiy;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class Turnover implements Parcelable {

    @SerializedName("id")
    private String id;
    @SerializedName("canaryId")
    private String canaryId;
    @SerializedName("boxCount")
    private String boxCount;
    @SerializedName("bottleCount")
    private String bottleCount;
    @SerializedName("date")
    private String date;
    @SerializedName("wineName")
    private String wineName;
    @SerializedName("status_id")
    private String status_id;
    @SerializedName("userId")
    private String userId;

    public Turnover() {
    }

    protected Turnover(Parcel in) {
        id = in.readString();
        canaryId = in.readString();
        boxCount = in.readString();
        bottleCount = in.readString();
        date = in.readString();
        wineName = in.readString();
        status_id = in.readString();
        userId = in.readString();
    }

    public static Creator<Turnover> getCREATOR() {
        return CREATOR;
    }

    public static final Creator<Turnover> CREATOR = new Creator<Turnover>() {
        @Override
        public Turnover createFromParcel(Parcel in) {
            return new Turnover(in);
        }

        @Override
        public Turnover[] newArray(int size) {
            return new Turnover[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCanaryId() {
        return canaryId;
    }

    public void setCanaryId(String canaryId) {
        this.canaryId = canaryId;
    }

    public String getBoxCount() {
        return boxCount;
    }

    public void setBoxCount(String boxCount) {
        this.boxCount = boxCount;
    }

    public String getBottleCount() {
        return bottleCount;
    }

    public void setBottleCount(String bottleCount) {
        this.bottleCount = bottleCount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getWineName() {
        return wineName;
    }

    public void setWineName(String wineName) {
        this.wineName = wineName;
    }

    public String getStatus_id() {
        return status_id;
    }

    public void setStatus_id(String status_id) {
        this.status_id = status_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(canaryId);
        dest.writeString(boxCount);
        dest.writeString(bottleCount);
        dest.writeString(date);
        dest.writeString(wineName);
        dest.writeString(status_id);
        dest.writeString(userId);
    }
}
