package woxapp.com.woxapptestapplication.helper.entitiy;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class WineInStock implements Parcelable {
    @SerializedName("total")
    private String total;
    @SerializedName("inbox")
    private String inbox;
    @SerializedName("bottle")
    private String bottle;
    @SerializedName("userId")
    private String userId;


    public WineInStock() {
    }

    protected WineInStock(Parcel in) {
        total = in.readString();
        inbox = in.readString();
        bottle = in.readString();
        userId = in.readString();
    }

    public static final Creator<WineInStock> CREATOR = new Creator<WineInStock>() {
        @Override
        public WineInStock createFromParcel(Parcel in) {
            return new WineInStock(in);
        }

        @Override
        public WineInStock[] newArray(int size) {
            return new WineInStock[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getInbox() {
        return inbox;
    }

    public void setInbox(String inbox) {
        this.inbox = inbox;
    }

    public String getBottle() {
        return bottle;
    }

    public void setBottle(String bottle) {
        this.bottle = bottle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(total);
        dest.writeString(inbox);
        dest.writeString(bottle);
        dest.writeString(userId);
    }
}
