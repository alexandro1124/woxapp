package woxapp.com.woxapptestapplication.model.adapters.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import woxapp.com.woxapptestapplication.R;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class ReminderHolder extends RecyclerView.ViewHolder {
    TextView remData, countText, text, wineName;
    ImageView image;
    FrameLayout coloredFrame;//here set red def color  of bg

    public ReminderHolder(View itemView) {
        super(itemView);
        image = (ImageView) itemView.findViewById(R.id.image);
        remData = (TextView) itemView.findViewById(R.id.remData);
        countText = (TextView) itemView.findViewById(R.id.countText);
        text = (TextView) itemView.findViewById(R.id.text);
        wineName = (TextView) itemView.findViewById(R.id.wineName);
        coloredFrame = (FrameLayout) itemView.findViewById(R.id.coloredFrame);
    }

    public TextView getRemData() {
        return remData;
    }

    public TextView getCountText() {
        return countText;
    }

    public TextView getText() {
        return text;
    }

    public TextView getWineName() {
        return wineName;
    }

    public ImageView getImage() {
        return image;
    }

    public FrameLayout getColoredFrame() {
        return coloredFrame;
    }
}
