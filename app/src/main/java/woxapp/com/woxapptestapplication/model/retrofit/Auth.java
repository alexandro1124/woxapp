package woxapp.com.woxapptestapplication.model.retrofit;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;


public interface Auth {

    @POST("auth")
    Observable<String> loginRx(@Body RequestBody requestBody);

    @POST("auth")
    Call<String> login(@Body RequestBody requestBody);


}