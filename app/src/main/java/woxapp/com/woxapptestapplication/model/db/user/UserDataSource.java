package woxapp.com.woxapptestapplication.model.db.user;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import woxapp.com.woxapptestapplication.helper.entitiy.UserDB;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class UserDataSource {
    private SQLiteDatabase database;
    private UserDBHelper dbHelper;
    private String[] allColumns = {UserDBHelper.COLUMN_ID,
            UserDBHelper.ACCESS_TOKEN, UserDBHelper.CELLAR_ID, UserDBHelper.IMEI};

    public UserDataSource(Context context) {
        dbHelper = new UserDBHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    /*
    *here i check if user will be in db
    * its strange to checke on acces token and cellar it,
    * but i have no another available data to check on unique
     */
    public UserDB createUser(UserDB userDB) {
        UserDB user;

        Cursor cursor1 = database.rawQuery("SELECT * FROM " + UserDBHelper.TABLE_USERS
                + " WHERE " + UserDBHelper.ACCESS_TOKEN + " = '" + userDB.getToken()
                + "' AND " + UserDBHelper.CELLAR_ID + " = '" + userDB.getCellarId()+"'", new String[]{});
        if (cursor1 != null && cursor1.getCount() > 0) {
            cursor1.moveToFirst();
            user = cursorToUser(cursor1);
            cursor1.close();
        } else {
            ContentValues values = new ContentValues();
            values.put(UserDBHelper.ACCESS_TOKEN, userDB.getToken());
            values.put(UserDBHelper.CELLAR_ID, userDB.getCellarId());
            values.put(UserDBHelper.IMEI, userDB.getImei());
            long insertId = database.insert(UserDBHelper.TABLE_USERS, null,
                    values);
            Cursor cursor = database.query(UserDBHelper.TABLE_USERS,
                    allColumns, UserDBHelper.COLUMN_ID + " = " + insertId, null,
                    null, null, null);
            cursor.moveToFirst();
            user = cursorToUser(cursor);
            cursor.close();
        }

        return user;
    }

    public void deleteUser(UserDB comment) {
        long id = comment.getId();
        System.out.println("Comment deleted with id: " + id);
        database.delete(UserDBHelper.TABLE_USERS, UserDBHelper.COLUMN_ID
                + " = " + id, null);
    }

    public UserDB getCurrentUser(String accesToken) {
        UserDB userDB = null;
        Cursor cursor = database.rawQuery("SELECT * FROM " + UserDBHelper.TABLE_USERS + "WHERE " + UserDBHelper.ACCESS_TOKEN + "=?", new String[]{accesToken + ""});
        try {
            cursor.moveToFirst();
            userDB = cursorToUser(cursor);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return userDB;
    }

    private UserDB cursorToUser(Cursor cursor) {
        UserDB userDB = new UserDB();
        userDB.setId(cursor.getLong(0));
        userDB.setToken(cursor.getString(1));
        userDB.setCellarId(cursor.getString(2));
        userDB.setImei(cursor.getString(3));
        return userDB;
    }
}
