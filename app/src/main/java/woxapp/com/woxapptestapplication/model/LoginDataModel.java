package woxapp.com.woxapptestapplication.model;

import android.content.Context;
import android.util.Log;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.subjects.BehaviorSubject;
import woxapp.com.woxapptestapplication.BuildConfig;
import woxapp.com.woxapptestapplication.model.di.AppComponent;
import woxapp.com.woxapptestapplication.model.retrofit.Auth;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class LoginDataModel {
    private static final String TAG = LoginDataModel.class.getSimpleName();
    private static Retrofit retrofit;
    private static Observable<String> loginResult;
    private static BehaviorSubject<String> observableModelsList;
    private static Subscription subscription;

    public LoginDataModel(Context con, AppComponent appComponent) {
        this.retrofit = appComponent.getRetrofit();

    }

    private static RequestBody convertFields(final String login, final String pass, String imei) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Datamodel convertFields()   " + login + " / " + pass + " / " + imei);
        }
        String bodyString = "{\"login\":\"" + login
                + "\",\"password\":\"" + pass
                + "\",\"imei\":\"" + imei
                + "\"}";
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), bodyString);
        return requestBody;
    }

    private static RequestBody convertStaticFields(final String login, final String pass, String imei) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Datamodel convertStaticFields()   admin / 123456 / 12345");
        }
        String bodyString = "{\"login\":\"" + "admin"
                + "\",\"password\":\"" + "123456"
                + "\",\"imei\":\"" + "12345"
                + "\"}";
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), bodyString);
        return requestBody;
    }

    public static void init(RequestBody requestBody) {
        Auth auth = retrofit.create(Auth.class);
        loginResult = auth.loginRx(requestBody);
    }

    public static void resetModelsObservable(final String login, final String pass, String imei) {
        observableModelsList = BehaviorSubject.create();

//        init(convertFields(login, pass, imei));

        init(convertStaticFields(login, pass, imei)); //TODO change this after test

        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
        subscription = loginResult.subscribe(new Subscriber<String>() {
            @Override
            public void onCompleted() {
                //do nothing
            }

            @Override
            public void onError(Throwable e) {
                observableModelsList.onError(e);
            }

            @Override
            public void onNext(String models) {
                observableModelsList.onNext(models);
            }
        });
    }

    public static Observable<String> getModelsObservable(final String login, final String pass, String imei) {
        if (observableModelsList == null) {
            resetModelsObservable(login, pass, imei);
        }
        return observableModelsList;
    }

}
