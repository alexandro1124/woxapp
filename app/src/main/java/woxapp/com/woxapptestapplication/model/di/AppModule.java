package woxapp.com.woxapptestapplication.model.di;

import android.content.Context;
import android.net.ConnectivityManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import rx.schedulers.Schedulers;
import woxapp.com.woxapptestapplication.model.db.dashboard.ReminderDataSource;
import woxapp.com.woxapptestapplication.model.db.dashboard.TurnoverDataSource;
import woxapp.com.woxapptestapplication.model.db.dashboard.WineInStokDataSource;
import woxapp.com.woxapptestapplication.model.db.user.UserDataSource;

/**
 * Created by Alexandro on 23.10.2016.
 */
@Module
public class AppModule {

    private final Context context;
    private final Retrofit retrofit;
    private final UserDataSource userDataSource;
    private final WineInStokDataSource wineInStokDataSource;
    private final ReminderDataSource reminderDataSource;
    private final TurnoverDataSource turnoverDataSource;

    public AppModule(Context context) {
        this.context = context;
        this.userDataSource = new UserDataSource(context);

        this.wineInStokDataSource = new WineInStokDataSource(context);
        this.reminderDataSource = new ReminderDataSource(context);
        this.turnoverDataSource = new TurnoverDataSource(context);
        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());

        this.retrofit = new Retrofit.Builder()
                .baseUrl("http://wine-cellar.biznestext.com/api/v1/")
                .addConverterFactory(ScalarsConverterFactory.create())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addCallAdapterFactory(rxAdapter)
                .build();
    }

    @Provides
    @Singleton
    public WineInStokDataSource getWineInStokDataSource() {
        return wineInStokDataSource;
    }

    @Provides
    @Singleton
    public ReminderDataSource getReminderDataSource() {
        return reminderDataSource;
    }

    @Provides
    @Singleton
    public TurnoverDataSource getTurnoverDataSource() {
        return turnoverDataSource;
    }

    @Provides
    @Singleton
    public UserDataSource getUserDataSource() {
        return userDataSource;
    }

    @Provides
    @Singleton
    public Context getContext() {
        return context;
    }

    @Provides
    @Singleton
    public ConnectivityManager getConnectivityManager() {
        return (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Provides
    @Singleton
    public Retrofit getRetrofit() {
        return retrofit;
    }

}
