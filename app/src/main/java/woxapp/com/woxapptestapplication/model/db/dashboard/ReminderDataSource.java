package woxapp.com.woxapptestapplication.model.db.dashboard;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import woxapp.com.woxapptestapplication.helper.entitiy.Reminder;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class ReminderDataSource {
    private final String TAG = ReminderDBHelper.class.getSimpleName();
    private SQLiteDatabase database;
    private ReminderDBHelper dbHelper;
    private String[] allColumns = {ReminderDBHelper.COLUMN_ID,
            ReminderDBHelper.USER_ID,
            ReminderDBHelper.CANARYID,
            ReminderDBHelper.DATE,
            ReminderDBHelper.WINENAME,
            ReminderDBHelper.BOXCOUNT,
            ReminderDBHelper.BOTTLECOUNT,
            ReminderDBHelper.REMINDERTYPE};

    public ReminderDataSource(Context context) {
        dbHelper =   dbHelper.getInstance(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void createReminder(Reminder reminder, String userId) {
        ContentValues values = new ContentValues();
        values.put(ReminderDBHelper.USER_ID, userId);
        values.put(ReminderDBHelper.CANARYID, reminder.getCanaryId());
        values.put(ReminderDBHelper.DATE, reminder.getDate());
        values.put(ReminderDBHelper.WINENAME, reminder.getWineName());
        values.put(ReminderDBHelper.BOXCOUNT, reminder.getBoxCount());
        values.put(ReminderDBHelper.BOTTLECOUNT, reminder.getBottleCount());
        values.put(ReminderDBHelper.TEXT, reminder.getText());
        values.put(ReminderDBHelper.REMINDERTYPE, reminder.getReminderType());
        long insertId = database.insert(ReminderDBHelper.TABLE_REMINDER, null,
                values);
        Log.e(TAG, "createReminder " + reminder.getCanaryId() + " | " + userId);
//        Cursor cursor = database.query(ReminderDBHelper.TABLE_REMINDER,
//                allColumns, ReminderDBHelper.COLUMN_ID + " = " + insertId, null,
//                null, null, null);
//        cursor.moveToFirst();
//        Reminder reminder1 = cursorToReminder(cursor);
//        cursor.close();
//        return reminder1;
    }


    public boolean getCurrentReminder(Reminder reminder, String userID) {

        Log.e(TAG, "getCurrentReminder " + reminder.getCanaryId() + " | " + userID);
        Cursor cursor = database.rawQuery("SELECT * FROM " + ReminderDBHelper.TABLE_REMINDER
                + " WHERE " + ReminderDBHelper.CANARYID + " = '" + reminder.getCanaryId() + "'"
                + " AND " + ReminderDBHelper.USER_ID + " = '" + userID + "'", new String[]{});
        boolean cantInsert = false;
        try {
            if (cursor != null && cursor.getCount() > 0) {
                cantInsert = true;
            } else {
                createReminder(reminder, userID);
            }
        } catch (CursorIndexOutOfBoundsException e) {
            e.printStackTrace();

        } finally {
            cursor.close();
        }
        return cantInsert;
    }

    public void deleteReminder(Reminder comment) {
        long id = Long.parseLong(comment.getId());
        database.delete(ReminderDBHelper.TABLE_REMINDER, ReminderDBHelper.COLUMN_ID
                + " = " + id, null);
    }

    public List<Reminder> getAllReminders(String userId) {
        List<Reminder> reminderArrayList = new ArrayList<Reminder>();
        Cursor cursor = database.query(ReminderDBHelper.TABLE_REMINDER,
                allColumns, ReminderDBHelper.USER_ID + " = '" + userId+"'", null,
                null,
                null
                , ReminderDBHelper.DATE + " DESC");
//        Cursor cursor = database.rawQuery("SELECT * FROM " + ReminderDBHelper.TABLE_REMINDER
////                +" WHERE " + ReminderDBHelper.USER_ID + " = '" + userId + "'"
////                +" ORDER BY date(" + ReminderDBHelper.DATE + ") DESC"
//                , new String[]{""});

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Reminder reminder = cursorToReminder(cursor);
            reminderArrayList.add(reminder);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return reminderArrayList;
    }


    private Reminder cursorToReminder(Cursor cursor) {
        Reminder reminder = new Reminder();
        reminder.setId(String.valueOf(cursor.getLong(0)));
        reminder.setUserId(cursor.getString(1));
        reminder.setCanaryId(cursor.getString(2));
        reminder.setDate(cursor.getString(3));
        reminder.setWineName(cursor.getString(4));
        reminder.setBoxCount(cursor.getString(5));
        reminder.setBottleCount(cursor.getString(6));
        reminder.setReminderType(cursor.getString(7));
        return reminder;
    }
}
