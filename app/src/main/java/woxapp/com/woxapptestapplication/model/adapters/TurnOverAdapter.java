package woxapp.com.woxapptestapplication.model.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import woxapp.com.woxapptestapplication.R;
import woxapp.com.woxapptestapplication.helper.entitiy.CombinedTurnover;
import woxapp.com.woxapptestapplication.helper.entitiy.Turnover;
import woxapp.com.woxapptestapplication.model.adapters.holders.TurnoverHolder;
import woxapp.com.woxapptestapplication.view.MainActivity;

/**
 * Created by Alexandro on 23.10.2016.
 * status id = 0  -coming(left)
 * status id = 1 - consumption(right)
 * and divider=3
 */

public class TurnOverAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final List<CombinedTurnover> turnovers;
    private final MainActivity mainActivity;
    private final GetTurnoverClickItem getClickItem;

    public TurnOverAdapter(MainActivity mainActivity, GetTurnoverClickItem getClickItem) {
        this.turnovers = new ArrayList<>();
        this.mainActivity = mainActivity;
        this.getClickItem = getClickItem;
    }


    public void add(List<CombinedTurnover> item) {
        for (CombinedTurnover CombinedTurnover : item) {
            turnovers.add(CombinedTurnover);
        }
        notifyDataSetChanged();
    }

    public void clear() {
        turnovers.clear();
        notifyDataSetChanged();
    }

    public void updateItem(int pos) {
        CombinedTurnover cc = turnovers.get(pos);
        turnovers.remove(pos);
        turnovers.add(pos, cc);
        notifyItemChanged(pos);
    }

    public CombinedTurnover getItemPosition(int position) {
        return turnovers.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v1 = inflater.inflate(R.layout.statistic_item_main, parent, false);
        viewHolder = new TurnoverHolder(v1);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final TurnoverHolder vh = (TurnoverHolder) holder;
        CombinedTurnover turnoverHolder = getItemPosition(position);
        Turnover turnover;
        if (turnoverHolder.getComing() != null) {
            turnover = turnoverHolder.getComing();
            vh.getDataL().setText(turnover.getDate());
            vh.getWineNameL().setText(turnover.getWineName());
            if (turnover.getBottleCount() != null && Integer.valueOf(turnover.getBottleCount()) != 0) {
                vh.getBottleTextL().setText(turnover.getBottleCount());
                vh.getBottleItemL().setVisibility(View.VISIBLE);
            }
            if (turnover.getBoxCount() != null && Integer.valueOf(turnover.getBoxCount()) != 0) {
                vh.getBoxTextL().setText(turnover.getBoxCount());
                vh.getBoxItemL().setVisibility(View.VISIBLE);
            }
            vh.getLeftStLayout().setVisibility(View.VISIBLE);
        }
        if (turnoverHolder.getConsumption() != null) {
            turnover = turnoverHolder.getConsumption();
            vh.getDataR().setText(turnover.getDate());
            vh.getWineNameR().setText(turnover.getWineName());
            if (turnover.getBottleCount() != null && Integer.valueOf(turnover.getBottleCount()) != 0) {
                vh.getBottleTextR().setText(turnover.getBottleCount());
                vh.getBottleItemR().setVisibility(View.VISIBLE);
            }
            if (turnover.getBoxCount() != null && Integer.valueOf(turnover.getBoxCount()) != 0) {
                vh.getBoxTextR().setText(turnover.getBoxCount());
                vh.getBoxItemR().setVisibility(View.VISIBLE);
            }
            vh.getRightStLayout().setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return turnovers.size();
    }

    public interface GetTurnoverClickItem {
        void onTurnItemClick(View v, int pos);
    }
}
