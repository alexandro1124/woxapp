package woxapp.com.woxapptestapplication.model.db.dashboard;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import woxapp.com.woxapptestapplication.helper.entitiy.UserDB;
import woxapp.com.woxapptestapplication.helper.entitiy.WineInStock;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class WineInStokDataSource {
    private final String TAG = WineInStokDataSource.class.getSimpleName();
    private SQLiteDatabase database;
    private WineInStokDBHelper dbHelper;
    private String[] allColumns = {WineInStokDBHelper.COLUMN_ID,
            WineInStokDBHelper.USER_ID,
            WineInStokDBHelper.BOTTLE,
            WineInStokDBHelper.INBOX,
            WineInStokDBHelper.TOTAL};

    public WineInStokDataSource(Context context) {
        dbHelper = dbHelper.getInstance(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void createWineInStock(WineInStock userDB, String userId) {
        ContentValues values = new ContentValues();
        values.put(WineInStokDBHelper.USER_ID, userId);
        values.put(WineInStokDBHelper.BOTTLE, userDB.getBottle());
        values.put(WineInStokDBHelper.INBOX, userDB.getInbox());
        values.put(WineInStokDBHelper.TOTAL, userDB.getTotal());
        long insertId = database.insert(WineInStokDBHelper.TABLE_WINE_IN_STOCK, null,
                values);
        Log.e(TAG, "createWineInStock " + insertId + " | " + userId);
//        Cursor cursor = database.query(WineInStokDBHelper.TABLE_WINE_IN_STOCK,
//                allColumns, WineInStokDBHelper.COLUMN_ID + " = " + insertId, null,
//                null, null, null);
//        cursor.moveToFirst();
//        WineInStock wineInStock = cursorToWine(cursor);
//        cursor.close();
//        return wineInStock;
    }


    public boolean checkWineInStock(WineInStock wineInStock, String userID) {

        Cursor cursor = database.rawQuery("SELECT * FROM " + WineInStokDBHelper.TABLE_WINE_IN_STOCK
                + " WHERE " + WineInStokDBHelper.USER_ID + " = '" + userID + "'", new String[]{});
        boolean cantInsert = false;
        try {
            if (cursor != null && cursor.getCount() > 0) {
                cantInsert = true;
            } else {
                createWineInStock(wineInStock, userID);
            }
        } catch (CursorIndexOutOfBoundsException e) {
            e.printStackTrace();

        } finally {
            cursor.close();
        }
        return cantInsert;
    }

    public void deleteUser(UserDB comment) {
        long id = comment.getId();
        System.out.println("Comment deleted with id: " + id);
        database.delete(WineInStokDBHelper.TABLE_WINE_IN_STOCK, WineInStokDBHelper.COLUMN_ID
                + " = " + id, null);
    }

    public WineInStock getAllData(String userId) {
        WineInStock wineInStock1 = null;
        Cursor cursor = database.rawQuery("SELECT * FROM " + WineInStokDBHelper.TABLE_WINE_IN_STOCK
                + " WHERE " + WineInStokDBHelper.USER_ID + " = '" + userId + "'", new String[]{});
        try {
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                wineInStock1 = cursorToWine(cursor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return wineInStock1;
    }

    private WineInStock cursorToWine(Cursor cursor) {
        WineInStock wineInStock = new WineInStock();
        wineInStock.setUserId(cursor.getString(1));
        wineInStock.setBottle(cursor.getString(2));
        wineInStock.setInbox(cursor.getString(3));
        wineInStock.setTotal(cursor.getString(4));
        return wineInStock;
    }
}
