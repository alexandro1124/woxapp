package woxapp.com.woxapptestapplication.model.db.dashboard;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import woxapp.com.woxapptestapplication.model.db.user.UserDBHelper;

/**
 * Created by Alexandro on 23.10.2016.
 */

public final class TurnOverDBHelper extends SQLiteOpenHelper {
    public static final String TABLE_TURNOVER = "TurnOver";
    public static final String COLUMN_ID = "_id";
    public static final String USER_ID = "user_id";
    public static final String CANARYID = "canaryId";
    public static final String BOXCOUNT = "boxCount";
    public static final String BOTTLECOUNT = "bottleCount";
    public static final String DATE = "date";
    public static final String WINENAME = "wineName";
    public static final String STATUS_ID = "status_id";

    private static final String DATABASE_NAME = "TurnOverWineCellar.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_TURNOVER + "( " + COLUMN_ID
            + " integer primary key autoincrement, "
            + USER_ID + " text not null, "
            + CANARYID + " text , "
            + BOXCOUNT + " text , "
            + BOTTLECOUNT + " text , "
            + DATE + " text , "
            + WINENAME + " text , "
            + STATUS_ID + " text );";

    public TurnOverDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(UserDBHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TURNOVER);
        onCreate(db);
    }

    private static volatile TurnOverDBHelper turnInstance;

    public static synchronized TurnOverDBHelper getInstance(Context context) {
        if (turnInstance == null) {
            synchronized (TurnOverDBHelper.class) {
                if (turnInstance == null) {
                    turnInstance = new TurnOverDBHelper(context.getApplicationContext());
                }
            }

        }
        return turnInstance;
    }
}
