package woxapp.com.woxapptestapplication.model;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import woxapp.com.woxapptestapplication.helper.entitiy.CombinedTurnover;
import woxapp.com.woxapptestapplication.helper.entitiy.Turnover;
import woxapp.com.woxapptestapplication.model.db.dashboard.TurnoverDataSource;
import woxapp.com.woxapptestapplication.model.di.AppComponent;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class TurnOverDataModel extends DBModel {
    private static final String TAG = TurnOverDataModel.class.getSimpleName();
    TurnoverDataSource turnoverDataSource;
    private static Subscription subscription;
    Observable<Boolean> o2;


    public TurnOverDataModel(AppComponent appComponent) {
        turnoverDataSource = appComponent.getTurnoverDataSource();
    }


    /*
    *Make this custom crazy search  that output value would be more attractive
    * because now as i see from api i have random generated values
    * and if i am only sort by data and show this value it will be like "snake"
    * 0 | -
    * - | 0
    * 0 | -
    * (here i am show "snake", where 0 is data from coming data from api
    * and - its empty place)
    *
    * Now i am sort data for some check state, where standing next to dates
    * are equal and have come and consumption values(0 and 1)
    * if this statement is true then i am place this two values into 1 element
    * and then display it in 1 row, it will be more attractive as i think and
    * out put must be like
    * 0 | 0
    * - | 0
    * 0 | 0
    * 0 | -
    * 0 | 0
    * 0 | -
    * ( where 0 is data from coming data from api
    * and - its empty place)
    *
    * If do not use this "magic super sort" the i can show this data in List<Turnover>
    */
    @Override
    public Observable<List<CombinedTurnover>> getAllData(final String userId) {

        return Observable.defer(new Func0<Observable<List<CombinedTurnover>>>() {
            @Override
            public Observable<List<CombinedTurnover>> call() {
                turnoverDataSource.open();
                final List<Turnover> list = turnoverDataSource.getAllTurnOver(userId);
                turnoverDataSource.close();
                Log.e(TAG, "list is " + list.size());
                List<CombinedTurnover> combinedTurnovers = new ArrayList<CombinedTurnover>();
                for (int i = 0; i < list.size(); i++) {
                    CombinedTurnover combinedTurnover = new CombinedTurnover();
                    Turnover nowTurnover, nextTurnOver;
                    SimpleDateFormat formatInput = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    SimpleDateFormat formatOutput = new SimpleDateFormat("dd-MM-yyyy");
                    int nowStId, nextStId;
                    if (i + 1 > list.size() - 1) {
                        nowTurnover = list.get(i);
                        nowStId = Integer.valueOf(nowTurnover.getStatus_id());
                        Date date1 = null;
                        try {
                            date1 = formatInput.parse(nowTurnover.getDate());
                            Log.e(TAG,"data inserted1 "+nowTurnover.getDate());
                            nowTurnover.setDate(formatOutput.format(date1));// change date to readable format
                            Log.e(TAG,"data inserted2 "+nowTurnover.getDate());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if (nowStId == 0) {
                            combinedTurnover.setComing(nowTurnover);
                        } else {
                            combinedTurnover.setConsumption(nowTurnover);
                        }
                        combinedTurnovers.add(combinedTurnover);
                        continue;
                    }

                    nowTurnover = list.get(i);
                    nextTurnOver = list.get(i + 1);

                    nowStId = Integer.valueOf(nowTurnover.getStatus_id());
                    nextStId = Integer.valueOf(nextTurnOver.getStatus_id());

                    try {
                        Date date1 = formatInput.parse(nowTurnover.getDate());
                        Date date2 = formatInput.parse(nextTurnOver.getDate());
                        Log.e(TAG,"data inserted1 "+nowTurnover.getDate());
                        nowTurnover.setDate(formatOutput.format(date1));// change date to readable format
                        Log.e(TAG,"data inserted2 "+nowTurnover.getDate());
                        if (date1.equals(date2) &&
                                ((nowStId == 0) && (nextStId == 1))) {
                            combinedTurnover.setComing(nowTurnover);
                            combinedTurnover.setConsumption(nextTurnOver);
                            combinedTurnovers.add(combinedTurnover);
                            continue;
                        } else {
                            if (nowStId == 0) {
                                combinedTurnover.setComing(nowTurnover);
                            } else {
                                combinedTurnover.setConsumption(nowTurnover);
                            }
                            combinedTurnovers.add(combinedTurnover);
                            continue;
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }

                return Observable.just(combinedTurnovers);
            }
        });
    }

    @Override
    void insertOrUpdate(Object listInput, final String userID) {
        Log.e(TAG, "list size " + ((List) listInput).size());
        Observable<Turnover> o1 = Observable.from(((List) listInput)).subscribeOn(Schedulers.computation());
        o2 = o1.flatMap(new Func1<Turnover, Observable<Boolean>>() {
            @Override
            public Observable<Boolean> call(Turnover turnover) {
                turnoverDataSource.open();
                boolean b = turnoverDataSource.getCurrentTurnOver(turnover, userID);
                turnoverDataSource.close();
                return null;
            }
        }).subscribeOn(Schedulers.io());

        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }

        subscription = o2.subscribe(new Observer() {
            @Override
            public void onCompleted() {
                Log.e(TAG, "onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "error", e);

            }

            @Override
            public void onNext(Object o) {

            }

        });
    }

    @Override
    public Observable<Boolean> getInsertObservable(Object listInput, String userID) {
        if (o2 == null) {
            insertOrUpdate(listInput, userID);
        }
        return o2;
    }


}
