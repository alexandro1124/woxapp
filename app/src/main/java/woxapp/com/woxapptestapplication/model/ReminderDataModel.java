package woxapp.com.woxapptestapplication.model;

import android.util.Log;

import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import woxapp.com.woxapptestapplication.helper.entitiy.Reminder;
import woxapp.com.woxapptestapplication.model.db.dashboard.ReminderDataSource;
import woxapp.com.woxapptestapplication.model.di.AppComponent;

/**
 * Created by Alexandro on 27.10.2016.
 */

public class ReminderDataModel extends DBModel {
    private static final String TAG = ReminderDataModel.class.getSimpleName();
    ReminderDataSource reminderDataSource;
    private static Subscription subscription;
    Observable<Boolean> o2;

    public ReminderDataModel(AppComponent appComponent) {
        reminderDataSource = appComponent.getReminderDataSource();
    }


    @Override
    public Object getAllData(String userId) {
        reminderDataSource.open();
        List<Reminder> list = reminderDataSource.getAllReminders(userId);
        reminderDataSource.close();
        Log.e(TAG, "list is " + list.size());
        return list;
    }

    @Override
    public void insertOrUpdate(Object listInput, final String userID) {
        Log.e(TAG, "list size " + ((List) listInput).size());
        Observable<Reminder> o1 = Observable.from(((List) listInput)).subscribeOn(Schedulers.computation());
        o2 = o1.flatMap(new Func1<Reminder, Observable<Boolean>>() {
            @Override
            public Observable<Boolean> call(Reminder reminder) {
                reminderDataSource.open();
                boolean b = reminderDataSource.getCurrentReminder(reminder, userID);
                reminderDataSource.close();
                return null;
            }
        }).subscribeOn(Schedulers.io());


        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }

        subscription = o2.subscribe(new Observer() {
            @Override
            public void onCompleted() {
                Log.e(TAG, "onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "error", e);

            }

            @Override
            public void onNext(Object o) {

            }

        });
    }

    @Override
    public Observable<Boolean> getInsertObservable(Object listInput, String userID) {
        if (o2 == null) {
            insertOrUpdate(listInput, userID);
        }
        return o2;
    }
}
