package woxapp.com.woxapptestapplication.model.retrofit;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Alexandro on 23.10.2016.
 */

public interface DashBoard {

    @GET("dashboard")
    Observable<String> dashboardRx(@Query("imei") String imei,
                                   @Query("access_token") String access_token,
                                   @Query("cellar_id") String cellar_id);
}
