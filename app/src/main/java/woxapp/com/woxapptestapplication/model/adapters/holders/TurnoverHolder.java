package woxapp.com.woxapptestapplication.model.adapters.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import woxapp.com.woxapptestapplication.R;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class TurnoverHolder extends RecyclerView.ViewHolder {
    TextView dataR, boxTextR, bottleTextR, wineNameR;
    LinearLayout bottleItemR, boxItemR;
    TextView dataL, boxTextL, bottleTextL, wineNameL;
    LinearLayout bottleItemL, boxItemL;

    View rightStLayout, leftStLayout;

    public TurnoverHolder(View itemView) {
        super(itemView);
        leftStLayout = itemView.findViewById(R.id.leftStLayout);
        rightStLayout = itemView.findViewById(R.id.rightStLayout);
        dataR = (TextView) rightStLayout.findViewById(R.id.data);
        boxTextR = (TextView) rightStLayout.findViewById(R.id.boxText);
        bottleTextR = (TextView) rightStLayout.findViewById(R.id.bottleText);
        wineNameR = (TextView) rightStLayout.findViewById(R.id.wineName);
        boxItemR = (LinearLayout) rightStLayout.findViewById(R.id.boxItem);
        bottleItemR = (LinearLayout) rightStLayout.findViewById(R.id.bottleItem);

        dataL = (TextView) leftStLayout.findViewById(R.id.data);
        boxTextL = (TextView) leftStLayout.findViewById(R.id.boxText);
        bottleTextL = (TextView) leftStLayout.findViewById(R.id.bottleText);
        wineNameL = (TextView) leftStLayout.findViewById(R.id.wineName);
        boxItemL = (LinearLayout) leftStLayout.findViewById(R.id.boxItem);
        bottleItemL = (LinearLayout) leftStLayout.findViewById(R.id.bottleItem);

    }

    public TextView getDataR() {
        return dataR;
    }

    public TextView getBoxTextR() {
        return boxTextR;
    }

    public TextView getBottleTextR() {
        return bottleTextR;
    }

    public TextView getWineNameR() {
        return wineNameR;
    }

    public LinearLayout getBottleItemR() {
        return bottleItemR;
    }

    public LinearLayout getBoxItemR() {
        return boxItemR;
    }

    public TextView getDataL() {
        return dataL;
    }

    public TextView getBoxTextL() {
        return boxTextL;
    }

    public TextView getBottleTextL() {
        return bottleTextL;
    }

    public TextView getWineNameL() {
        return wineNameL;
    }

    public LinearLayout getBottleItemL() {
        return bottleItemL;
    }

    public LinearLayout getBoxItemL() {
        return boxItemL;
    }

    public View getRightStLayout() {
        return rightStLayout;
    }

    public View getLeftStLayout() {
        return leftStLayout;
    }


}
