package woxapp.com.woxapptestapplication.model;

import android.util.Log;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.functions.Func0;
import rx.schedulers.Schedulers;
import woxapp.com.woxapptestapplication.helper.entitiy.WineInStock;
import woxapp.com.woxapptestapplication.model.db.dashboard.WineInStokDataSource;
import woxapp.com.woxapptestapplication.model.di.AppComponent;

/**
 * Created by Alexandro on 27.10.2016.
 */

public class WineInStockDataModel extends DBModel {

    private static final String TAG = WineInStockDataModel.class.getSimpleName();
    WineInStokDataSource wineInStokDataSource;
    private static Subscription subscription;
    Observable<Boolean> o2;

    public WineInStockDataModel(AppComponent appComponent) {
        wineInStokDataSource = appComponent.getWineInStokDataSource();
    }


    @Override
    public Object getAllData(String userId) {
        wineInStokDataSource.open();
        WineInStock allData = wineInStokDataSource.getAllData(userId);
        wineInStokDataSource.close();
        return allData;
    }

    @Override
    public void insertOrUpdate(final Object wineInStock, final String userID) {
        Log.e(TAG, "WineInStock   "   + userID);
        o2 = Observable.defer(new Func0<Observable<Boolean>>() {
            @Override
            public Observable<Boolean> call() {
                wineInStokDataSource.open();
                boolean b = wineInStokDataSource.checkWineInStock(((WineInStock) wineInStock), userID);
                wineInStokDataSource.close();
                return null;
            }
        }).subscribeOn(Schedulers.io());


        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }

        subscription = o2.subscribe(new Observer() {
            @Override
            public void onCompleted() {

                Log.e(TAG, "onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "error", e);

            }

            @Override
            public void onNext(Object o) {

            }

        });
    }


    @Override
    public Observable<Boolean> getInsertObservable(Object wineInStock, String userID) {
        if (o2 == null) {
            insertOrUpdate(wineInStock, userID);
        }
        return o2;
    }
}
