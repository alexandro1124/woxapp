package woxapp.com.woxapptestapplication.model.db.user;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class UserDBHelper extends SQLiteOpenHelper {
    public static final String TABLE_USERS = "Users";
    public static final String COLUMN_ID = "_id";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String CELLAR_ID = "cellar_id";
    public static final String IMEI = "imei";
    private static final String DATABASE_NAME = "UserWineCellar.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_USERS + "( " + COLUMN_ID
            + " integer primary key autoincrement, "
            + ACCESS_TOKEN + " text not null, "
            + CELLAR_ID + " text not null, "
            + IMEI + " text not null );";

    public UserDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(UserDBHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        onCreate(db);
    }

}
