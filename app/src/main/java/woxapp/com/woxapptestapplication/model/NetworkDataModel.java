package woxapp.com.woxapptestapplication.model;

import android.content.Context;

import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.subjects.BehaviorSubject;
import woxapp.com.woxapptestapplication.model.di.AppComponent;
import woxapp.com.woxapptestapplication.model.retrofit.DashBoard;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class NetworkDataModel {
    private static final String TAG = LoginDataModel.class.getSimpleName();
    private static Retrofit retrofit;
    private static Observable<String> dashbordResult;
    private static BehaviorSubject<String> observableDashbordList;
    private static Subscription subscription;


    public NetworkDataModel(Context con, AppComponent appComponent) {
        this.retrofit = appComponent.getRetrofit();
    }


    public static void init(String imei, String token, String celarId) {
        DashBoard dashboard = retrofit.create(DashBoard.class);
        dashbordResult = dashboard.dashboardRx(imei, token, celarId);
    }

    public static void resetDashbordObservable(String imei, String token, String celarId) {
        observableDashbordList = BehaviorSubject.create();
        init(imei, token, celarId);

        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
        subscription = dashbordResult.subscribe(new Subscriber<String>() {
            @Override
            public void onCompleted() {
                //do nothing
            }

            @Override
            public void onError(Throwable e) {
                observableDashbordList.onError(e);
            }

            @Override
            public void onNext(String models) {
                observableDashbordList.onNext(models);
            }
        });
    }

    public static Observable<String> getDashbordObservable(String imei, String token, String celarId) {
        if (observableDashbordList == null) {
            resetDashbordObservable(imei, token, celarId);
        }
        return observableDashbordList;
    }


}
