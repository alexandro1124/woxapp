package woxapp.com.woxapptestapplication.model.db.dashboard;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import woxapp.com.woxapptestapplication.helper.entitiy.Turnover;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class TurnoverDataSource {
    private static final String TAG = TurnoverDataSource.class.getSimpleName();
    private SQLiteDatabase database;
    private TurnOverDBHelper dbHelper;
    private String[] allColumns = {TurnOverDBHelper.USER_ID,
            TurnOverDBHelper.CANARYID,
            TurnOverDBHelper.BOXCOUNT,
            TurnOverDBHelper.BOTTLECOUNT,
            TurnOverDBHelper.DATE,
            TurnOverDBHelper.WINENAME,
            TurnOverDBHelper.STATUS_ID};

    public TurnoverDataSource(Context context) {

        dbHelper = dbHelper.getInstance(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }


    public boolean getCurrentTurnOver(Turnover turnover, String userID) {

        Log.e(TAG, "getCurrentTurnover " + turnover.getCanaryId() + " | " + userID);
        Cursor cursor = database.rawQuery("SELECT * FROM " + TurnOverDBHelper.TABLE_TURNOVER
                + " WHERE " + TurnOverDBHelper.CANARYID + " = '" + turnover.getCanaryId() + "'"
                + " AND " + TurnOverDBHelper.USER_ID + " = '" + userID + "'", new String[]{});
        boolean cantInsert = false;
        try {
            if (cursor != null && cursor.getCount() > 0) {
                cantInsert = true;
            } else {
                createTurnOver(turnover, userID);
            }
        } catch (CursorIndexOutOfBoundsException e) {
            e.printStackTrace();

        } finally {
            cursor.close();
        }
        return cantInsert;
    }

    public void createTurnOver(Turnover turnover, String userID) {

        ContentValues values = new ContentValues();
        values.put(TurnOverDBHelper.USER_ID, userID);
        values.put(TurnOverDBHelper.CANARYID, turnover.getCanaryId());
        values.put(TurnOverDBHelper.BOXCOUNT, turnover.getBoxCount());
        values.put(TurnOverDBHelper.BOTTLECOUNT, turnover.getBottleCount());
        values.put(TurnOverDBHelper.DATE, turnover.getDate());
        values.put(TurnOverDBHelper.WINENAME, turnover.getWineName());
        values.put(TurnOverDBHelper.STATUS_ID, turnover.getStatus_id());

        long insertId = database.insert(TurnOverDBHelper.TABLE_TURNOVER, null,
                values);
        Log.e(TAG, "createTurnOver " + turnover.getCanaryId() + " | " + userID);
    }

    public void deleteTurnOver(Turnover turnover) {
        long id = Long.parseLong(turnover.getId());
        System.out.println("Comment deleted with id: " + id);
        database.delete(TurnOverDBHelper.TABLE_TURNOVER, TurnOverDBHelper.COLUMN_ID
                + " = " + id, null);
    }


    public List<Turnover> getAllTurnOver(String userId) {

        List<Turnover> reminderArrayList = new ArrayList<Turnover>();
        Cursor cursor = database.rawQuery("SELECT * FROM " + TurnOverDBHelper.TABLE_TURNOVER +
                " WHERE " + TurnOverDBHelper.USER_ID + " = '" + userId + "'" +
                " ORDER BY date(" + TurnOverDBHelper.DATE + ") DESC", new String[]{});

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Turnover reminder = cursorToTurnover(cursor);
            reminderArrayList.add(reminder);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return reminderArrayList;
    }

    public Turnover cursorToTurnover(Cursor cursor) {
        Turnover turnover = new Turnover();
        turnover.setId(String.valueOf(cursor.getLong(0)));
        turnover.setUserId(cursor.getString(1));
        turnover.setCanaryId(cursor.getString(2));
        turnover.setBoxCount(cursor.getString(3));
        turnover.setBottleCount(cursor.getString(4));
        turnover.setDate(cursor.getString(5));
        turnover.setWineName(cursor.getString(6));
        turnover.setStatus_id(cursor.getString(7));

        return turnover;
    }


}
