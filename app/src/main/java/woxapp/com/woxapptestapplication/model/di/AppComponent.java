package woxapp.com.woxapptestapplication.model.di;

import android.content.Context;
import android.net.ConnectivityManager;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;
import woxapp.com.woxapptestapplication.model.db.dashboard.ReminderDataSource;
import woxapp.com.woxapptestapplication.model.db.dashboard.TurnoverDataSource;
import woxapp.com.woxapptestapplication.model.db.dashboard.WineInStokDataSource;
import woxapp.com.woxapptestapplication.model.db.user.UserDataSource;
import woxapp.com.woxapptestapplication.presenter.LoginPresenter;
import woxapp.com.woxapptestapplication.presenter.MainPresenter;

/**
 * Created by Alexandro on 23.10.2016.
 */

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(LoginPresenter activity);

    void inject(MainPresenter activity);

    ConnectivityManager getConMan();

    Retrofit getRetrofit();

    UserDataSource getUserDataSource();

    WineInStokDataSource getWineInStokDataSource();

    ReminderDataSource getReminderDataSource();

    TurnoverDataSource getTurnoverDataSource();

    Context getContext();
}
