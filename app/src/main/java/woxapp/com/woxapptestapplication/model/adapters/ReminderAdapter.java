package woxapp.com.woxapptestapplication.model.adapters;

import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import woxapp.com.woxapptestapplication.R;
import woxapp.com.woxapptestapplication.helper.entitiy.Reminder;
import woxapp.com.woxapptestapplication.model.adapters.holders.ReminderHolder;
import woxapp.com.woxapptestapplication.view.MainActivity;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class ReminderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<Reminder> reminders;
    private final MainActivity mainActivity;
    private final GetReminderClickItem getClickItem;

    public ReminderAdapter(MainActivity mainActivity, GetReminderClickItem getClickItem) {
        this.reminders = new ArrayList<>();
        this.mainActivity = mainActivity;
        this.getClickItem = getClickItem;
    }


    public void add(List<Reminder> item) {
        for (Reminder Reminder : item) {
            reminders.add(Reminder);
        }
        notifyDataSetChanged();
    }

    public void clear() {
        reminders.clear();
        notifyDataSetChanged();
    }

    public void updateItem(int pos) {
        Reminder cc = reminders.get(pos);
        reminders.remove(pos);
        reminders.add(pos, cc);
        notifyItemChanged(pos);
    }

    public Reminder getItemPosition(int position) {
        return reminders.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v1 = inflater.inflate(R.layout.notofications_item, parent, false);
        viewHolder = new ReminderHolder(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ReminderHolder vh = (ReminderHolder) holder;
        Reminder reminder = getItemPosition(position);
        if (reminder != null) {
            if (reminder.getBottleCount() != null && reminder.getBottleCount().length() > 0) {
                vh.getCountText().setText(reminder.getBottleCount());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    vh.getImage().setImageDrawable(mainActivity.getResources().getDrawable(R.drawable.ic_bottle_black, null));
                } else {
                    vh.getImage().setImageDrawable(mainActivity.getResources().getDrawable(R.drawable.ic_bottle_black));

                }
            } else {
                vh.getCountText().setText(reminder.getBoxCount());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    vh.getImage().setImageDrawable(mainActivity.getResources().getDrawable(R.drawable.ic_boxes_pictogram, null));
                } else {
                    vh.getImage().setImageDrawable(mainActivity.getResources().getDrawable(R.drawable.ic_boxes_pictogram));

                }

            }
            if (position == 0) {//perhaps need detect item type
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    vh.getColoredFrame().setBackground(mainActivity.getResources().getDrawable(R.drawable.red_item_bg, null));
                } else {
                    vh.getColoredFrame().setBackground(mainActivity.getResources().getDrawable(R.drawable.red_item_bg));
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    vh.getColoredFrame().setBackground(mainActivity.getResources().getDrawable(R.drawable.green_item_bg, null));
                } else {
                    vh.getColoredFrame().setBackground(mainActivity.getResources().getDrawable(R.drawable.green_item_bg));
                }
            }
            vh.getRemData().setText(reminder.getDate());
            vh.getWineName().setText(reminder.getWineName());
            vh.getText().setText(reminder.getText());
        }
    }

    @Override
    public int getItemCount() {
        return reminders.size();
    }

    public interface GetReminderClickItem {
        void onRemItemClick(View v, int pos);
    }
}
