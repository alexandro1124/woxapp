package woxapp.com.woxapptestapplication.model;

import rx.Observable;

/**
 * Created by Alexandro on 27.10.2016.
 */

public  abstract class DBModel <T> {


    public   abstract T getAllData(String userId);

    abstract void insertOrUpdate(T listInput, final String userID);

    public abstract Observable<Boolean> getInsertObservable(T input, final String userID);


}
