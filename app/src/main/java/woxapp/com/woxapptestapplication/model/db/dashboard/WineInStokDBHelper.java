package woxapp.com.woxapptestapplication.model.db.dashboard;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import woxapp.com.woxapptestapplication.model.db.user.UserDBHelper;

/**
 * Created by Alexandro on 23.10.2016.
 */

public final class WineInStokDBHelper extends SQLiteOpenHelper {
    public static final String TABLE_WINE_IN_STOCK = "MainInfo";
    public static final String COLUMN_ID = "_id";
    public static final String USER_ID = "user_id";
    public static final String TOTAL = "total";
    public static final String INBOX = "inbox";
    public static final String BOTTLE = "bottle";
    private static final String DATABASE_NAME = "MainWineCellar.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_WINE_IN_STOCK + "( " + COLUMN_ID
            + " integer primary key autoincrement, "
            + USER_ID + " text not null, "
            + BOTTLE + " text , "
            + INBOX + " text , "
            +  TOTAL+ " text  );";

    public WineInStokDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(UserDBHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WINE_IN_STOCK);
        onCreate(db);
    }

    private static volatile WineInStokDBHelper wineInStokDBHelper;

    public static synchronized WineInStokDBHelper getInstance(Context context) {
        if (wineInStokDBHelper == null) {
            synchronized (WineInStokDBHelper.class) {
                if (wineInStokDBHelper == null) {
                    wineInStokDBHelper = new WineInStokDBHelper(context.getApplicationContext());
                }
            }

        }
        return wineInStokDBHelper;
    }
}
