package woxapp.com.woxapptestapplication;

import android.app.Application;

import woxapp.com.woxapptestapplication.model.di.AppComponent;
import woxapp.com.woxapptestapplication.model.di.AppModule;
import woxapp.com.woxapptestapplication.model.di.DaggerAppComponent;


/**
 * Created by Alexandro on 23.10.2016.
 */

public class App extends Application {

    private AppComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this ))
                .build();

    }

    public AppComponent getComponent() {
        return applicationComponent;
    }
}
