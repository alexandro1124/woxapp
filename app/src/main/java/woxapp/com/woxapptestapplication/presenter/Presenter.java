package woxapp.com.woxapptestapplication.presenter;

/**
 * Created by Alexandro on 04.10.2016.
 */

public interface Presenter {
    void onCreate();
    void onPause();
    void onResume();
    void onDestroy();
}
