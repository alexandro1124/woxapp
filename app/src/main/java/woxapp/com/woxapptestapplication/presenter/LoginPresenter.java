package woxapp.com.woxapptestapplication.presenter;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.provider.Settings.System;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import woxapp.com.woxapptestapplication.App;
import woxapp.com.woxapptestapplication.BuildConfig;
import woxapp.com.woxapptestapplication.R;
import woxapp.com.woxapptestapplication.helper.entitiy.UserDB;
import woxapp.com.woxapptestapplication.model.LoginDataModel;
import woxapp.com.woxapptestapplication.model.db.user.UserDataSource;
import woxapp.com.woxapptestapplication.model.di.AppComponent;
import woxapp.com.woxapptestapplication.view.LoginActivity;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class LoginPresenter implements Presenter {
    private final LoginActivity loginActivity;
    private final String TAG = LoginPresenter.class.getSimpleName();

    LoginDataModel loginDataModel;
    @Inject
    ConnectivityManager connect;
    private AppComponent appComponent;
    private Subscription subscription;
    @Inject
    UserDataSource userDataSource;

    public LoginPresenter(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;

    }

    @Override
    public void onCreate() {
        appComponent = ((App) loginActivity.getApplication()).getComponent();
        appComponent.inject(this);
        Context context = loginActivity.getApplicationContext();
        this.loginDataModel = new LoginDataModel(context, appComponent);
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }


    public boolean connect() {
        if (connect != null) {
            NetworkInfo activeNetwork = connect.getActiveNetworkInfo();
            return activeNetwork == null ? false : true;
        }
        return false;
    }

    public void getModelsList(String login, String pass, final String imei) {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
        loginActivity.showProgress(true);
        subscription = loginDataModel.getModelsObservable(login, pass, imei).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (BuildConfig.DEBUG) Log.d(TAG, "onError", e);
                        if (BuildConfig.DEBUG)
                            Log.d(TAG, "onError e.getMessage()" + e.getMessage());
                        if (BuildConfig.DEBUG)
                            Log.d(TAG, "onError e.getMessage()" + e.getLocalizedMessage());
                        loginActivity.showProgress(false);
                        ErrorDetector(e);
                    }

                    @Override
                    public void onNext(String inputData) {
                        loginActivity.showProgress(false);
                        if (BuildConfig.DEBUG) Log.d(TAG, "onNext: " + inputData);
                        UserDB userDB = SaveUserToDB(inputData,/*imei*/"12345");
                        loginActivity.openMainScreen(userDB);
                    }
                });
    }

    private UserDB SaveUserToDB(String inputData,String imei) {

        UserDB userDB = new UserDB();
        try {

            JSONObject userData = new JSONObject(inputData);

            userDB.setToken(userData.has("access_token") ? userData.getString("access_token") : null);
            userDB.setCellarId(userData.has("cellar_id") ? userData.getString("cellar_id") : null);
            userDB.setImei(imei);
            if (userDB.getToken() != null && userDB.getCellarId() != null) {
                userDataSource.open();
                userDB = userDataSource.createUser(userDB);
                userDataSource.close();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return userDB;
    }

    private void ErrorDetector(Throwable e) {
        if (e instanceof HttpException) {
            // We had non-2XX http error
            HttpException exception = (HttpException) e;
            if (exception != null) {

                switch (exception.code()) {
                    case 404:
                        loginActivity.showErrorMessage(loginActivity.getString(R.string.error_message_404));
                        break;
                    case 400:
                        loginActivity.showErrorMessage(loginActivity.getString(R.string.error_message_400));
                        break;
                    case 500:
                        loginActivity.showErrorMessage(loginActivity.getString(R.string.error_message_500));
                        break;
                    default:
                        loginActivity.showErrorMessage(loginActivity.getString(R.string.error_message_undefinite));
                        break;
                }
            }
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "onError HttpException" + ((HttpException) e).code());
                Log.d(TAG, "onError HttpException exception.code()" + exception.code());
                ResponseBody responseBody = exception.response().errorBody();
                try {
                    Log.d(TAG, "onError HttpException responseBody .string()" + responseBody.string());
                    JSONObject jObjError = new JSONObject(responseBody.string());
                    Log.d(TAG, "onError HttpException jObjError" + jObjError);
                } catch (JSONException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        } else {
            loginActivity.showErrorMessage(loginActivity.getString(R.string.error_message_undefinite));
        }
    }

    /*
    * This has the advantage that it doesn't require special permissions
    * but can change if another application has write access and changes
     * it (which is apparently unusual but not impossible).
     *
     * ANDROID_ID setting from Android.Provider.Settings.System
     * (as described here http://www.strazzere.com/blog/2008/11/uniquely-identifying-android-devices-without-special-permissions/).
     *
     * Its using only in case whet imei is null and need some unique identification
     * otherwise
     * Implementation note: if the ID is critical to the system architecture
     * you need to be aware that in practice some of the very low end Android
     * phones & tablets have been found reusing the same ANDROID_ID
     * (9774d56d682e549c was the value showing up in our logs)
     */
    private String loadAnotherWayUnique() {
        return System.getString(loginActivity.getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}
