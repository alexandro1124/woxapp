package woxapp.com.woxapptestapplication.presenter;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.MainThread;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import woxapp.com.woxapptestapplication.App;
import woxapp.com.woxapptestapplication.BuildConfig;
import woxapp.com.woxapptestapplication.R;
import woxapp.com.woxapptestapplication.helper.JSONParser;
import woxapp.com.woxapptestapplication.helper.entitiy.CombinedTurnover;
import woxapp.com.woxapptestapplication.helper.entitiy.DashboardData;
import woxapp.com.woxapptestapplication.helper.entitiy.Reminder;
import woxapp.com.woxapptestapplication.helper.entitiy.Turnover;
import woxapp.com.woxapptestapplication.helper.entitiy.UserDB;
import woxapp.com.woxapptestapplication.helper.entitiy.WineInStock;
import woxapp.com.woxapptestapplication.model.NetworkDataModel;
import woxapp.com.woxapptestapplication.model.ReminderDataModel;
import woxapp.com.woxapptestapplication.model.TurnOverDataModel;
import woxapp.com.woxapptestapplication.model.WineInStockDataModel;
import woxapp.com.woxapptestapplication.model.di.AppComponent;
import woxapp.com.woxapptestapplication.view.MainActivity;

/**
 * Created by Alexandro on 23.10.2016.
 */

public class MainPresenter implements Presenter {

    private static final String TAG = MainPresenter.class.getSimpleName();
    UserDB userDB;
    NetworkDataModel model;
    private AppComponent appComponent;
    private Subscription subscriptionNetwork, subscriptionDataBase;
    private final MainActivity mainActivity;
    TurnOverDataModel turnOverDataModel;
    ReminderDataModel reminderDataModel;
    WineInStockDataModel wineInStockDataModel;
    @Inject
    ConnectivityManager connect;

    public MainPresenter(MainActivity mainActivity) {
        this.mainActivity = mainActivity;

    }

    public void setUserDb(UserDB userDB) {
        this.userDB = userDB;
        Log.e(TAG, "presenter is  user  " + userDB.getToken() + "/ " + userDB.getCellarId());
    }

    public UserDB getUserDb() {
        return
                userDB;
    }

    @Override
    public void onCreate() {
        appComponent = ((App) mainActivity.getApplication()).getComponent();
        appComponent.inject(this);
        model = new NetworkDataModel(mainActivity.getApplication(), appComponent);
        turnOverDataModel = new TurnOverDataModel(appComponent);
        reminderDataModel = new ReminderDataModel(appComponent);
        wineInStockDataModel = new WineInStockDataModel(appComponent);
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        if (subscriptionNetwork != null && !subscriptionNetwork.isUnsubscribed()) {
            subscriptionNetwork.unsubscribe();
        }
        if (subscriptionDataBase != null && !subscriptionDataBase.isUnsubscribed()) {
            subscriptionDataBase.unsubscribe();
        }
    }

    public boolean connect() {
        if (connect != null) {
            NetworkInfo activeNetwork = connect.getActiveNetworkInfo();
            return activeNetwork == null ? false : true;
        }
        return false;
    }


    public void getModelsList() {
        mainActivity.changeLoadTextWine(mainActivity.getString(R.string.loading_data));
        mainActivity.changeLoadTextRem(mainActivity.getString(R.string.loading_data));
        mainActivity.changeLoadTextStat(mainActivity.getString(R.string.loading_data));

        if (!connect()) {
            mainActivity.showErrorMessage(mainActivity.getString(R.string.error_message_no_internet));
            showAllLoading(false);
        }
        if (subscriptionNetwork != null && !subscriptionNetwork.isUnsubscribed()) {
            subscriptionNetwork.unsubscribe();
        }

        Log.e(TAG, "getModelList " + userDB.getImei() + " / " + userDB.getToken() + " / " + userDB.getCellarId());
        subscriptionNetwork = model.getDashbordObservable(userDB.getImei(), userDB.getToken(), userDB.getCellarId()).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (BuildConfig.DEBUG) Log.d(TAG, "onError", e);
                        if (BuildConfig.DEBUG)
                            Log.d(TAG, "onError e.getMessage()" + e.getMessage());
                        if (BuildConfig.DEBUG)
                            Log.d(TAG, "onError e.getMessage()" + e.getLocalizedMessage());
                        showAllLoading(false);
                        ErrorDetector(e);
                    }

                    @Override
                    public void onNext(String inputData) {
                        showAllLoading(false);
                        if (BuildConfig.DEBUG) Log.d(TAG, "onNext: " + inputData);
                        parseAndSave(inputData);


                    }
                });
    }

    private void parseAndSave(String inputData) {
        showAllLoading(true);
        final String userId = String.valueOf(userDB.getId());
        DashboardData dashboardData = new JSONParser().parseDashboard(inputData);
        Observable<Boolean> o3 = turnOverDataModel.getInsertObservable(dashboardData.getTurnover(), userId);
        Observable<Boolean> o2 = reminderDataModel.getInsertObservable(dashboardData.getReminder(), userId);
        Observable<Boolean> o1 = wineInStockDataModel.getInsertObservable(dashboardData.getWineInStock(), userId);
        Observable<Boolean>[] array = new Observable[]{o1, o2, o3};

        subscriptionDataBase = Observable.concat(o1, o2, o3)
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Boolean>() {

                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted");
                        showAllLoading(false);
                        showDbData(userId);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError", e);
                        showAllLoading(false);
                    }

                    @Override
                    public void onNext(Boolean booleanObservable) {
                        Log.d(TAG, "onNext " + booleanObservable);
                    }
                });


    }

    public boolean showDbData(String userId) {
        mainActivity.showProgresWineView(true);
        mainActivity.changeLoadTextWine(mainActivity.getString(R.string.sync_data));
        WineInStock allWineInStock = (WineInStock) wineInStockDataModel.getAllData(userId);
        if (allWineInStock != null) {
            // check wineinstok because its small and not require many time to fetch data
            // if there is null the its mean than there is no data in data base at all
            mainActivity.showWineInStock(allWineInStock);
            mainActivity.showProgresWineView(false);
            mainActivity.showProgresRemView(true);
            mainActivity.changeLoadTextRem(mainActivity.getString(R.string.sync_data));
            List<Reminder> allReminder = (List<Reminder>) reminderDataModel.getAllData(userId);

            mainActivity.showRemaining(allReminder);
            mainActivity.showProgresRemView(false);
            showCustomTurnover(userId);
            return true;
        }
        return false;
    }

    private void showCustomTurnover(String userId) {
        mainActivity.showProgresTurnView(true);
        mainActivity.changeLoadTextStat(mainActivity.getString(R.string.sync_data));
        Observable<List<CombinedTurnover>> combined = turnOverDataModel.getAllData(userId);

        combined.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<CombinedTurnover>>() {
                               @Override
                               public void onCompleted() {
                                   Log.d(TAG, "onCompleted");

                               }

                               @Override
                               public void onError(Throwable e) {
                                   Log.d(TAG, "onError", e);
                               }

                               @Override
                               public void onNext(List<CombinedTurnover> combinedTurnover) {
                                   Log.d(TAG, "onNext");
                                   mainActivity.showTurnOver(combinedTurnover);
                                   mainActivity.showProgresTurnView(false);
                               }
                           }

                );


    }

    /*
    * get data from database
    *  phase #1 - if data contains - show data - > load new data from net - save data to db
    *  phase #2 - if no data then load from network -> save to db ->show data
    *  phase #3 - if no connection try load from db if no data the show error else show data
    *
     */
    public void checkAllData() {
        showAllLoading(true);
        if (userDB == null) {
            showAllLoading(false);
            mainActivity.showErrorMessage(mainActivity.getString(R.string.error_message_user_not_found));
            return;
        }
        String userId = String.valueOf(userDB.getId());

        if (!showDbData(userId)) {
            getModelsList();
        } else {
            getModelsList();
        }

    }

    private void showAllLoading(boolean show) {
        mainActivity.showProgresRemView(show);
        mainActivity.showProgresTurnView(show);
        mainActivity.showProgresWineView(show);
    }

    private void ErrorDetector(Throwable e) {
        if (e instanceof HttpException) {
            // We had non-2XX http error
            HttpException exception = (HttpException) e;
            if (exception != null) {

                switch (exception.code()) {
                    case 404:
                        mainActivity.showErrorMessage(mainActivity.getString(R.string.error_message_404));
                        break;
                    case 400:
                        mainActivity.showErrorMessage(mainActivity.getString(R.string.error_message_400));
                        break;
                    case 500:
                        mainActivity.showErrorMessage(mainActivity.getString(R.string.error_message_500));
                        break;
                    default:
                        mainActivity.showErrorMessage(mainActivity.getString(R.string.error_message_undefinite));
                        break;
                }
            }
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "onError HttpException" + ((HttpException) e).code());
                Log.d(TAG, "onError HttpException exception.code()" + exception.code());
                ResponseBody responseBody = exception.response().errorBody();
                try {
                    Log.d(TAG, "onError HttpException responseBody .string()" + responseBody.string());
                    JSONObject jObjError = new JSONObject(responseBody.string());
                    Log.d(TAG, "onError HttpException jObjError" + jObjError);
                } catch (JSONException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        } else {
            mainActivity.showErrorMessage(mainActivity.getString(R.string.error_message_undefinite));
        }
    }
}
